# truthy

`truthy` is a tiny, MIT-licensed script you can use to check whether a string
represents "a true value", ie. is equal to 1, on, t, true, True, y, Y, yes or
Yes.


## Example

```sh
# in example.sh
if truthy "$ENABLE_SOME_FEATURE"; then
  printf "Feature enabled!\n";
else
  printf "Feature disabled\n"
fi
```

```sh
$ ./example.sh
Feature disabled
$ ENABLE_SOME_FEATURE=no ./example.sh
Feature disabled
$ ENABLE_SOME_FEATURE=yes ./example.sh
Feature enabled!
```


## Install

There is not much to install. Simply download/copy `truthy` and make it
executable.

For configuration management, I recommend you bundle `truthy` or download it
once manually, verify the code and then include a checksum in your source.
