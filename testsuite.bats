@test "true : 1" {
  run ./truthy 1
  [ "$status" -eq 0 ]
}

@test "true : on" {
  run ./truthy on
  [ "$status" -eq 0 ]
}

@test "true : t" {
  run ./truthy t
  [ "$status" -eq 0 ]
}

@test "true : true" {
  run ./truthy true
  [ "$status" -eq 0 ]
}

@test "true : True" {
  run ./truthy True
  [ "$status" -eq 0 ]
}

@test "true : y" {
  run ./truthy y
  [ "$status" -eq 0 ]
}

@test "true : Y" {
  run ./truthy Y
  [ "$status" -eq 0 ]
}

@test "true : yes" {
  run ./truthy yes
  [ "$status" -eq 0 ]
}

@test "true : Yes" {
  run ./truthy Yes
  [ "$status" -eq 0 ]
}

@test "false : 0" {
  run ./truthy 0
  [ "$status" -eq 1 ]
}

@test "false : off" {
  run ./truthy off
  [ "$status" -eq 1 ]
}

@test "false : f" {
  run ./truthy f
  [ "$status" -eq 1 ]
}

@test "false : false" {
  run ./truthy false
  [ "$status" -eq 1 ]
}

@test "false : False" {
  run ./truthy False
  [ "$status" -eq 1 ]
}

@test "false : n" {
  run ./truthy n
  [ "$status" -eq 1 ]
}

@test "false : N" {
  run ./truthy N
  [ "$status" -eq 1 ]
}

@test "false : no" {
  run ./truthy no
  [ "$status" -eq 1 ]
}

@test "false : No" {
  run ./truthy No
  [ "$status" -eq 1 ]
}

@test "false : something-else" {
  run ./truthy something-else
  [ "$status" -eq 1 ]
}

@test "usage 0 args" {
  run ./truthy
  [ "$output" = "Usage: ./truthy TEST" ]
}

@test "usage 2 args" {
  run ./truthy a b
  [ "$output" = "Usage: ./truthy TEST" ]
}
